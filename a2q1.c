//checks whether that the entered number is either positive or negative or zero.

#include <stdio.h>

int main()
{
    int num=0;

    printf("Enter number: ");
    scanf("%d", &num);

     if (num > 0){
        printf("%d is a positive number\n", num);
     }else if (num < 0){
        printf("%d is a negative number\n", num);
     }else{
        printf("You entered zero\n");
     }
    return 0;
}
