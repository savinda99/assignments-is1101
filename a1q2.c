#include <stdio.h>

int main()
{
    float pi=3.14;
    float radius;
    printf("Enter the radius: ");
    scanf("%f", &radius);
    printf("Area of the disk: %f\n", pi*radius*radius);
    return 0;
}
