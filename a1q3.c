#include <stdio.h>

int main()
{
    int x,y,z;
    printf("Enter integer for A: ");
    scanf("%d", &x);
    printf("Enter integer for B: ");
    scanf("%d", &y);
    printf("Swapping integers...\n");
    z=x;
    x=y;
    y=z;
    printf("A = %d\nB = %d\n", x,y);
    return 0;
}
