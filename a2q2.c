//takes a number from the user and chekcs whether that number is odd or even.

#include <stdio.h>

int main(){
	int num=0;
	int ans=0;

	printf("Enter number: ");
	scanf("%d", &num);

	ans = num%2;
//checks whether the entered number has a remainder when it's divided by two.
	(ans == 0) ? printf("%d is an even number\n", num) : printf("%d is an odd number\n", num);
	return 0;
}
