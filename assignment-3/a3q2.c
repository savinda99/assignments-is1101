//determine the given number is a prime number

#include <stdio.h>

int main()
{
    int num=0, i=0, j=1;

    printf("Enter a number: ");
    scanf("%d", &num);

    while(num>=j){
        if(num%j==0) i++;
        j++;
    }
    if(i==2){
        printf("%d is a prime number\n", num);
    }else{
        printf("%d is not a prime number\n", num);
        }
    return 0;
}


