//calculate the sum of all given positive integers until we enter the 0 or a negative value.

#include <stdio.h>

int main()
{
    int num=0, total=0;

    do{
        printf("Enter an integer: ");
        scanf("%d", &num);
        if(num<=0) break;
        total+=num;

    }while(num>0);
        printf("Total: %d\n", total);

    return 0;
}
