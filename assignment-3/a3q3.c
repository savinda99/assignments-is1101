//Reverse a Number

#include <stdio.h>

int main()
{
    int num=0, rnum=0, i=0;

    printf("Enter a number: ");
    scanf("%d", &num);

    while(num!=0){
        i=num%10;
        rnum=rnum*10+i;
        num/=10;
        }
    printf("Reversed number is: %d", rnum);
    return 0;
}


