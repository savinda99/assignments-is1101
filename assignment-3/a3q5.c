//print multiplication tables from 1 to a given number

#include <stdio.h>

int main()
{
    int num=0, i=0, j=0;

    printf("Enter a number: ");
    scanf("%d", &num);

    for(i=1; i<=num; i++){
        for(j=1; j<=10; j++){
            printf("%d * %d = %d\n", i, j, i*j);
        }
    printf("--------------\n");
    }
    return 0;
}



