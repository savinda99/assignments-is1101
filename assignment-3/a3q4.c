//find factors of a given number

#include <stdio.h>

int main()
{
    int num=0, i=1;

    printf("Enter a number: ");
    scanf("%d", &num);
    printf("Factors of %d are: ", num);

    while(num>i){
        if(num%i==0) printf("%d, ", i);
        i++;
    }
    return 0;
    }


